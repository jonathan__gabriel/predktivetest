
## Log


```python
import logging
import logging.config
import json
```


```python
## Create logger
# https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/

pathLogFile = "./config/configLog.json"
with open(pathLogFile, 'rt') as f:
    config = json.load(f)
logging.config.dictConfig(config)

#logging.config.fileConfig("./config/configLog.json")
```


```python
logger = logging.getLogger("simpleExample4")
```


```python

logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
logger.critical('critical message')
```

    2019-01-25 18:20:25 | [<ipython-input-43-88d17a02d19b>:3] | INFO     | info message
    2019-01-25 18:20:25 | [<ipython-input-43-88d17a02d19b>:4] | WARNING  | warn message
    2019-01-25 18:20:25 | [<ipython-input-43-88d17a02d19b>:5] | ERROR    | error message
    2019-01-25 18:20:25 | [<ipython-input-43-88d17a02d19b>:6] | CRITICAL | critical message

